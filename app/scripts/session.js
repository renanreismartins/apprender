var Session = Session || {};

Session.createSession = function (socket) {
  
  var peerConnection = createConnection();

  var session = {
    socket : socket,
    peerConnection : peerConnection,

    join : function(room) {
      socket.emit('join', {room : room});
    },

    buildRTCSession : function (description) {
                        return new RTCSessionDescription(description);
                      },

    allowMedia : function() {
                   socket.emit('allowMedia');
                 }
  };

  MicroEvent.mixin(session);

  function startCall() {
    peerConnection.createOffer(function (description) {
      peerConnection.setLocalDescription(description, function () {
        socket.emit('new_offer', description);
      });
    }, onCreateSessionDescriptionError);
  };
  
  function onCreateSessionDescriptionError(error) {
    console.log('Failed to create session description: ' + error.toString());
  };

  socket.on('participantRole', function (isCaller) {
    if (isCaller) {
      startCall();
    }
  });

  socket.on('new_ice_candidate', function (ice_candidate) {
    if (ice_candidate.candidate) {
      peerConnection.addIceCandidate(new RTCIceCandidate(ice_candidate.candidate), onAddIceCandidateSuccess, onAddIceCandidateError);
    }
  });

  function onAddIceCandidateSuccess() {
      console.log('AddIceCandidate success.');
  };

  function onAddIceCandidateError(error) {
    console.log('Failed to add Ice Candidate: ' + error.toString());
  };

  socket.on('new_offer', function(offer) {
    peerConnection.setRemoteDescription(session.buildRTCSession(offer), onSetRemoteDescriptionSuccess, onSetSessionDescriptionError);

    peerConnection.createAnswer(function (description) {
      peerConnection.setLocalDescription(description, function () {
        socket.emit('new_answer', description);
      });
    }, onCreateSessionDescriptionError);
  });

  socket.on('new_answer', function(answer) {
    peerConnection.setRemoteDescription(session.buildRTCSession(answer));
  });

  function onSetRemoteDescriptionSuccess() {
    console.log("Set remote session description success.");
  };

  function onSetSessionDescriptionError(error) {
    console.log('Failed to set session description: ' + error.toString());
  };

  function createConnection() {
    var config = {iceServers: [{ url: "turn:gorst@192.241.177.104:3478", "credential": "hero" }]};
    var peerConnection = new RTCPeerConnection(config);

    peerConnection.onicecandidate = function (event) {
      if (event.candidate) {
        socket.emit('new_ice_candidate', event);
      }
    };

    peerConnection.onaddstream = function (event) {
      session.trigger("onRemoteVideo", event);
    };

    return peerConnection;
  }

  socket.on('amigoDesconectou', function () {
    peerConnection.close();
    peerConnection = createConnection();
    session.peerConnection = peerConnection;
    
    session.trigger("amigoDesconectou");
  });

  return session; 
};

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = Session;
} else {
  window.Session = Session;
}
