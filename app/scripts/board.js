var Board = Board || {};

Board.createBoard = function(paper, socket) {
  var Path = paper.Path;
  var Point = paper.Point;
  var tool = new paper.Tool();

  var paths = {
    size: function() {
            return Object.keys(this).length - 1;
          }
  };

  var myPath;

  var user = {};

  tool.onMouseDown = function(event) {
    var point = event.point;
    createPath(point);

    paths[myPath.name] = myPath;

    sendPoint(point, 'dragStartFromClient');
  };

  function createPath(point) {
    myPath = new Path();
    myPath.name = user.id + "_"  + myPath.id;
    myPath.strokeColor = '#137';
    myPath.add(point);
  };

  tool.onMouseDrag = function(event) {
    var point = event.point;
    paths[myPath.name].add(point);

    sendPoint(point, 'dragFromClient');
  };

  function sendPoint(point, event) {
    var pointRepresentation  = {pathName : myPath.name, point: {x: point.x, y: point.y}};

    socket.emit(event, pointRepresentation);
  }
  socket.on("connect", function(data) {
    user.id = socket.socket.sessionid;
  });

  socket.on('dragStartFromServer', function(data) {
    var point = new Point(data.point.x, data.point.y);

    var path = new Path();
    path.name = data.pathName;
    path.strokeColor = 'black';
    path.add(point); 

    paths[path.name] = path;
  });

  socket.on('dragFromServer', function(data) {
    var point = new Point(data.point.x, data.point.y); 
    paths[data.pathName].add(point); 

    paper.view.draw();
  });

  socket.on('undoFromServer', function(lastKey) {
    paths[lastKey].remove();
    delete paths[lastKey];

    paper.view.draw();
  });

  return {
    user: user,
    paths: paths,
    tool: tool,
    undo : function() {
      if(paths.size() > 0) {
        var lastKey = Object.keys(paths)[paths.size()];
        var lastPath = paths[lastKey];

        paths[lastKey].remove();
        delete paths[lastKey];

        paper.view.draw();

        socket.emit('undoFromServer', lastKey);

        return lastPath;
      }
    }
  };

};

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = Board;
} else {
  window.Board = Board;
}
