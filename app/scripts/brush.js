var Brush = Brush || {};

Brush.create = function(context, config) {

  function configure() {
    context.strokeStyle = config.color;
    context.lineCap = config.lineCap; 
  }

  return {
    configure : configure
  }
};

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = Brush;
} else {
  window.Brush = Brush;
}
