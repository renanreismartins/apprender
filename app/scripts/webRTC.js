var WebRTC = WebRTC || {};

WebRTC.create = function(options, socket) {
  var localVideo = document.getElementById(options.localVideo); 
  var remoteVideo = document.getElementById(options.remoteVideo); 
  var localStream = null;

  var session = Session.createSession(socket);
  session.bind('onRemoteVideo', function(event) {
    attachMediaStream(remoteVideo, event.stream);
  });

  session.bind('amigoDesconectou', function () {
    session.peerConnection.addStream(localStream);
  });

  var constraints = {
    video: {
             mandatory: {
                          maxWidth: 320,
                          maxHeight: 180
                        },

             optional : [
             { minWidth: 320 },
             { minHeigth: 320 }
             ]
           },

    audio: true
  };

  getUserMedia(constraints, function(stream) {
    attachMediaStream(localVideo, stream);
    transmitStream(stream);
  },
  handleUserMediaError);

  function transmitStream (stream) {
    session.peerConnection.addStream(stream);
    localStream = stream;
    session.allowMedia();
  }

  function handleUserMediaError(error) {
    console.log('navigator.getUserMedia error: ', error);
  }

  var webRTC = {
    session: session
  };

  MicroEvent.mixin(webRTC);
  return webRTC;
};
