$(document).ready(function(){
  var room = $.url().param('room');

  var socket = io.connect('http://162.243.206.167:8081/video');

 // var socket = io.connect('http://192.168.1.9:8081/video');
  socket.on('connect', function() {
    socket.emit('join', {room : room});
  });

  WebRTC.create({
    localVideo: 'localVideo',
    remoteVideo: 'remoteVideo'
  }, socket);

  var canvasElement = document.getElementById("canvas");
  canvasElement.width = window.innerWidth;
  canvasElement.height = window.innerHeight;

  paper.setup(canvasElement);
  var board = Board.createBoard(paper, socket);

  Grid.create(
    {
      paper: paper,
      height : canvasElement.height,
    width : canvasElement.width,
    blockHeight : 20,
    blockWidth: 20,
    color : '#eee'
    }
    ).draw();

  var undob = document.getElementById("undo");
  undob.addEventListener("click", function() {
    board.undo();
  });


  //$('#myModal').modal('show');
});
