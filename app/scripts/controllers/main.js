'use strict';

angular.module('apprenderApp')
  .controller('MainCtrl', function ($scope) {
    $scope.room = Math.random().toString(36).substring(7);
  });
