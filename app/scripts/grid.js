var Grid = Grid || {};

Grid.create = function(gridConfig) {
  var Point = gridConfig.paper.Point;
  function draw() {
    var line;

    for (var x = 0.5; x < gridConfig.width; x += gridConfig.blockWidth) {
      line = createPath();
      line.add(new Point(x, 0), new Point(x, gridConfig.height));
    }

    for (var y = 0.5; y < gridConfig.height; y += gridConfig.blockHeight) {
      line = createPath();
      line.add(new Point(0, y), new Point(gridConfig.width, y));
    }

  }

  function createPath() {
    var path = new gridConfig.paper.Path();
    path.strokeColor = gridConfig.color;

    return path;
  }

  return { draw : draw }
};

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = Grid;
} else {
  window.Grid = Grid;
}
