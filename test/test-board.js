var should = require('should');
var sinon = require('sinon');
var Board = require('../app/scripts/board');

describe("A board", function () {
  var socket;
  var board;
  var paper;
  var PATH_NAME = "789_1";

  beforeEach(function () {
    paper = {
      Tool: function() {},
      Point: function() {},
      Path: function() {
        return {
          id: 1,
          add: sinon.spy(),
          remove: sinon.spy()
        };
      },
      view: {draw: sinon.spy()}
    };

    socket = {
      functions: [],
      socket: {sessionid: "789"},
      on: function (event, callback) {
        this.functions[event] = callback;
      },
      emit: sinon.spy()
    };

    board = Board.createBoard(paper, socket);
  });

  describe("when being created", function () {
    var data = {pathName: PATH_NAME, point: {x: 1, y: 1}};

    it("should set the userId that cames from socket's sessionid", function () {
      socket.functions["connect"]();

      board.user.should.have.property("id", socket.sessionid);
    });

    it("should start draw remote paths", function () {
      socket.functions["dragStartFromServer"](data);

      var paths = board.paths;

      paths.size().should.equal(1);
      paths[PATH_NAME].name.should.equal(PATH_NAME);
      paths[PATH_NAME].add.calledWith(new paper.Point(1, 1)).should.be.true;
    });


    it("should add points to remote paths", function () {
      board.paths[PATH_NAME] = new paper.Path();

      socket.functions["dragFromServer"](data);

      board.paths[PATH_NAME].add.calledWith(new paper.Point(1, 1)).should.be.true;
      paper.view.draw.called.should.be.true;
    });


    it("should remove remote path", function () {
      var paths = board.paths;
      var path = new paper.Path();
      paths[PATH_NAME] = path;
      paths["secondPathName"] = new paper.Path();

      socket.functions["undoFromServer"](PATH_NAME);

      path.remove.called.should.be.true;
      paths.size().should.equal(1);
      paths["secondPathName"].should.be.ok;
    });
  });

  describe("when drawing locally", function () {
    beforeEach(function () {
      board.user.id = 789;
    });
    
    var onMouseDownEvent = {
      point: {
        x: 1,
        y: 1
      }
    };

    var onMouseDragEvent = {
      point: {
        x: 2,
        y: 2
      }
    };

    it("should start a path", function () {
      board.tool.onMouseDown(onMouseDownEvent);

      board.paths[PATH_NAME].name.should.equal(PATH_NAME);
      board.paths[PATH_NAME].add.calledWith(onMouseDownEvent.point).should.be.true;
      socket.emit.calledWith("dragStartFromClient", {pathName: PATH_NAME, point: onMouseDownEvent.point}).should.be.true;
    });

    it("should add point to the local path", function() {
      board.tool.onMouseDown(onMouseDownEvent);
      board.tool.onMouseDrag(onMouseDragEvent);

      board.paths[PATH_NAME].add.calledWith(onMouseDragEvent.point).should.be.true;
      socket.emit.calledWith("dragFromClient", {pathName: PATH_NAME, point: onMouseDragEvent.point}).should.be.true;
    });
  });

  describe("when there is a path", function () {
    var onMouseDownEvent = {
      point: {
        x: 1,
        y: 1
      }
    };

    it("the path should be undo", function () {
      board.user.id = '789';
      var lastKey = '789_1';
      board.tool.onMouseDown(onMouseDownEvent);

      var lastPath = board.undo();

      lastPath.remove.called.should.be.true;
      board.paths.size().should.equal(0);
      paper.view.draw.called.should.be.true;
      socket.emit.calledWith('undoFromServer', lastKey).should.be.true;
    });
  });

  describe('when there is no path', function () {
    it('should not undo', function () {
      var lastKey = '789_1';

      var lastPath = board.undo();

      should(lastPath).eql(undefined);
      board.paths.size().should.equal(0);
      paper.view.draw.called.should.be.false;
      socket.emit.calledWith('undoFromServer', lastKey).should.be.false;
    });
  });
});
