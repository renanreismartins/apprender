var should = require('should');
var sinon = require('sinon');
var io = require('socket.io-client');
var Session = require('../../app/scripts/session');
var socketURL = 'http://localhost:8081/video';

var options ={
  transports: ['websocket'],
  'force new connection': true
};

describe("Video Session",function(){

  var socket1;
  var socket2;

  beforeEach(function(done) {
    setTimeout(function(){
      socket1 = io.connect(socketURL, options);
      socket2 = io.connect(socketURL, options);
      done();
    }, 900);
  });

  afterEach(function() {
    socket1.disconnect();
    socket2.disconnect();
  });

  it('should set the remoteDescription on the Callee and Caller', function(done){

    function asserts() {
      localPeerConnection1.setRemoteDescription.calledWith(description2).should.be.true;
      localPeerConnection2.setRemoteDescription.calledWith(description1).should.be.true;
      done();
    }

    var description1 = "description1";
    var description2 = "description2";

    var localPeerConnection1 = {
      setLocalDescription: function(description, callback) {
                             callback(description);
                           },
    createAnswer: function(callback) {
                      callback(description1);
                    },
    setRemoteDescription: sinon.spy()
    };


    var localPeerConnection2 = {
      setLocalDescription: function(description, callback) {
                             callback(description);
                           },
      createOffer: function(callback) {
                     callback(description2);
                   },
      setRemoteDescription: sinon.spy(function() { 
                              asserts();
                            }) 
    }

    var s1 = Session.createSession(localPeerConnection1, socket1);
    var s2 = Session.createSession(localPeerConnection2, socket2);

    var stub  = sinon.stub();
    stub.withArgs('description2').returns('description2');
    stub.withArgs('description1').returns('description1');
    s1.buildRTCSession = stub;
    s2.buildRTCSession = stub;

    s1.join('room2');
    s1.allowMedia();

    setTimeout(function(){
      s2.join('room2');
      s2.allowMedia();
    }, 900);
  });
});
