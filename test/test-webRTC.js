describe("A webRTC", function() {
  it("should start a video call", function() {
    var webRTC = WebRTC.create({
        localVideo: "localVideo",
        remoteVideo: 'remoteVideo',
        socket: socket
    });

    webRTC.on("readyToCall", readyToCallCallback);
  });
});
