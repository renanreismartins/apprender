var io = require('socket.io').listen(8081, { log: false });

io.of('/video').on('connection', function (socket) {
  console.log('cliente conectado');

  var room;

  var totalParticipants = 2;

  socket.on('join', function (data) {
    room = data.room;

    socket.join(room);
    console.log('join');
  });

  socket.on('allowMedia', function() {
    socket.emit('participantRole', isTheLastAllowedParticipant());
  });

  function isTheLastAllowedParticipant() {
    return io.of('/video').clients(room).length == totalParticipants;
  };

  socket.on('new_ice_candidate', function(ice_candidate) {
    console.log('broadcast do ice candidate');
    console.log(ice_candidate);
    socket.broadcast.to(room).emit('new_ice_candidate', ice_candidate);
  });

  socket.on('new_offer', function(offer) {
    console.log('broadcast da offer');
    socket.broadcast.to(room).emit('new_offer', offer);
  });

  socket.on('new_answer', function(offer) {
    console.log('broadcast da answer');
    socket.broadcast.to(room).emit('new_answer', offer);
  });

  socket.on('disconnect', function() {
    socket.broadcast.to(room).emit('amigoDesconectou');
  });

  // BOARD
  socket.on('dragStartFromClient', function (data) {
    console.log('start');
    socket.broadcast.to(room).emit('dragStartFromServer', data);
  });

  socket.on('dragFromClient', function (data) {
    console.log('desenhando');
    socket.broadcast.to(room).emit('dragFromServer', data);
  });

  socket.on('undoFromServer', function(data) {
    socket.broadcast.to(room).emit('undoFromServer', data);
  });
});
